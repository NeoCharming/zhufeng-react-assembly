## 1. GitHub Actions 持续集成

### 1. 生成 Token

1. 登录 github 右上角头像 Settings
2. Developer settings
3. Personal access tokens
4. Generate new token
5. 建议名称（publishpackage）
6. 复制好生成的 token

### 2. 添加 Token

1. 到 github 项目页 -> Settings
2. secrets
3. New secret
4. 建议名称（ACCESS_TOKEN）
5. 粘贴 上一步生成的 token

### 3. 添加 Actions

1. 到 github 项目页
2. Actions
3. New workflow -> 点击 Simple(第一个) 就行
4. Set up this workflow

```yml
name: Build and Deploy
on: [push]
jobs:
  build-and-deploy:
    runs-on: ubuntu-latest
    steps:
      - name: Checkout 🛎️
        uses: actions/checkout@v2 # If you're using actions/checkout@v2 you must set persist-credentials to false in most cases for the deployment to work correctly.
        with:
          persist-credentials: false

      - name: Install and Build 🔧 # This example project is built using npm and outputs the result to the 'build' folder. Replace with the commands required to build your project, or remove this step entirely if your site is pre-built.
        run: |
          npm install
          npm run build-storybook

      - name: Deploy 🚀
        uses: JamesIves/github-pages-deploy-action@releases/v3
        with:
          GITHUB_TOKEN: ${{secrets.ACCESS_TOKEN}}
          BRANCH: gh-pages # The branch the action should deploy to.
          FOLDER: storybook-static # The folder the action should deploy.
```

### 4. 拉取 Action 代码

```shell
git pull
```

### 5. 完成

合并之后，每一次推送就会自动构建了。
[查看推送后的结果](https://liuzemei.github.io/mixin-ui-react)

## 2. githubCoverall 配置

### 1. 注册

1. 访问[coveralls](https://coveralls.io/)
2. 直接用 github 登录。同步后 打开 仓库 off -> on
3. DETAILS
4. 拷贝出 repo_token

### 2. 在项目中添加 TOKEN

1. 参照 1.2
2. 名称建议（COVERALLS_REPO_TOKEN）
3. 填入上一步中的 repo_token

### 3. 安装 converalls

```shell
yarn add coveralls
```

### 4. 添加命令

`package.json`

```json
  "scripts": {
    "coverall": "npm run coverage  && cat ./coverage/lcov.info | ./node_modules/coveralls/bin/coveralls.js && rm -rf ./coverage"
  },
```

### 5. 修改 githubActions

`.github/workflows/blank.yml`

```yml
name: Build and Deploy
on: [push]
jobs:
  build-and-deploy:
    runs-on: ubuntu-latest
    steps:
      - name: Checkout 🛎️
        uses: actions/checkout@v2 # If you're using actions/checkout@v2 you must set persist-credentials to false in most cases for the deployment to work correctly.
        with:
          persist-credentials: false

      - name: Install and Build 🔧 # This example project is built using npm and outputs the result to the 'build' folder. Replace with the commands required to build your project, or remove this step entirely if your site is pre-built.
        run: |
          npm install
          npm run build-storybook

      - name: Deploy 🚀
        uses: JamesIves/github-pages-deploy-action@releases/v3
        with:
          GITHUB_TOKEN: ${{secrets.ACCESS_TOKEN}}
          BRANCH: gh-pages # The branch the action should deploy to.
          FOLDER: storybook-static # The folder the action should deploy.
      - name: Coveralls
         env:
           COVERALLS_SERVICE_NAME: 'GitHub CI'
           COVERALLS_GIT_BRANCH: master
           COVERALLS_REPO_TOKEN : ${{secrets.COVERALLS_REPO_TOKEN}}
         run: |
           npm run coverall
```

### 6. 添加 badges

1. 访问[coveralls](https://coveralls.io/)
2. 点击自己的项目
3. 最下边有个按钮 EMBED
4. 复制 MARKDOWN 里边的内容
5. 粘贴在自己的项目中 readme.md
6. git push 就能看到链接了。
