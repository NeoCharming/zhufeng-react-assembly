import React from "react";
import logo from "./logo.svg";
import "./App.css";

// import { ButtonProps } from "mixin-ui-react";
import { GlobalStyle, Button, Avatar } from "mixin-ui-react";
function App() {
  return (
    <div className="App">
      <GlobalStyle></GlobalStyle>
      <Button appearance="primary">2222</Button>
      <Avatar isLoading={true} />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React1
        </a>
      </header>
    </div>
  );
}

export default App;
