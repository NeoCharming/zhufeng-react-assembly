
import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "./Button";

export default {
  title: "Example/Button",
  component: Button,
};

export const Text = () => (
  <Button onClick={action("clicked")}>Hello Button</Button>
);

export const Emoji = () => (
  <Button onClick={action("clicked")}>hello storybook</Button>
);
export const Haha = () => (
  <Button onClick={action("clicked")}>hello haha</Button>
);