## 日期选择框

今天来做一下日期选择框，按道理本来做这个日期选择框之前应该把input和气泡框组件给封装好，不过不封也没事，不用考虑别的组件的逻辑了。

所以，做这个东西大致拆成2步：

1、制作input+弹框。

2、制作日历，并塞给弹框。

### input+弹框

我们先要实现个效果，就是当鼠标移入input并点击可以产生弹框。点击组件外，可以关闭弹窗。

由于我们需要判定点击组件外，所以需要一个hook：

```typescript
export function useClickOutside(
	ref: RefObject<HTMLElement>,
	handler: Function
) {
	useEffect(() => {
		const listener = (event: MouseEvent) => {
			if (!ref.current || ref.current.contains(event.target as Node)) {
				return;
			}
			handler(event);
		};
		window.addEventListener("click", listener);
		return () => window.removeEventListener("click", listener);
	}, [ref, handler]);
}
```

由于我们前面写过modal组件，直接拿modal的弹窗动画和当时做动画使用的useAnimation来完成日历的弹窗动画效果：

```typescript
const CalendarWrapper = styled.div<{ visible: boolean; delay: number }>`
	position: absolute;
	border: 1px solid black;
	transition: all ${(props) => props.delay / 1000}s cubic-bezier(0.23, 1, 0.32, 1);
background: ${color.lightest};
	${(props) =>
		props.visible &&
		css`
			animation: ${modalOpenAnimate} ${props.delay / 1000}s ease-in;
		`}
	${(props) =>
		!props.visible &&
		css`
			animation: ${modalCloseAnimate} ${props.delay / 1000}s ease-in;
		`}
`;

type DatepickerProps = {};

export function DatePicker(props: DatepickerProps) {
	const [state, setState] = useState("");
	const [show, setShow] = useState(false);
	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setState(e.target.value);
	};
	const handleClick = () => {
		setShow(true);
	};
	const ref = useRef<HTMLDivElement>(null);

	const [st, setst, unmount] = useStateAnimation(setShow, 200);
	useClickOutside(ref, () => setst(false));
	const render = useMemo(() => {
		if (!show) {
			unmount();
			return null;
		} else {
			return (
				<CalendarWrapper visible={st} delay={210}>
					我是弹框
				</CalendarWrapper>
			);
		}
	}, [show, unmount, st]);

	return (
		<div ref={ref}>
			<input
				value={state}
				onChange={handleChange}
				onClick={handleClick}
			></input>
			{render}
		</div>
	);
}
```

可以试一下，如果有打开关闭消失动画，第一步就算完成了。

### 制作日历

我们可以参考一下antd的日历组件，发现它都是6行7列，即使有几个月没满，甚至有一整行都是下个月的。所以数字部分做成6行7列。

我们把日历分为2部分，第一部分是日历头，用来切换月份年份，第二部分是主体，除了有日期还可能切换到年选择月选择。

下面梳理下关于日期的api。

地址：https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date

具体我们采用姜老师课上方法， 先看从当月1号往前移动7天，然后向后走42天即可。

首先把日历大致样子给做出来：

```typescript
const dayData = useMemo(() => {
		const arr = Array.from({ length: 6 }, () => new Array(7).fill(1));
		return arr;
	}, []);
	const render = useMemo(() => {
		if (!show) {
			unmount();
			return null;
		} else {
			return (
				<CalendarWrapper visible={st} delay={210}>
					<div>我是标题</div>
					<div>
						{dayData.map((v) => (
							<div>
								{v.map((k) => (
									<span>{k}</span>
								))}
							</div>
						))}
					</div>
				</CalendarWrapper>
			);
		}
	}, [show, unmount, st, dayData]);
```

由于有点丑，用styledcomponents加点样式：

```typescript
const CalendarDateRow = styled.tr``;

const tableItemStyle = css`
	display: inline-block;
	min-width: 24px;
	height: 24px;
	line-height: 24px;
	border-radius: 2px;
	margin: 2px;
	text-align: center;
`;
const CalendarHeadItem = styled.td`
	${tableItemStyle}
	cursor:default;
`;
const CalendarDate = styled.td`
	${tableItemStyle}
`;
const CalendarHeadWrapper = styled.div`
	padding: 10px;
	display: flex;
	color: ${rgba(0, 0, 0, 0.85)};
	border-bottom: 1px solid #f0f0f0;
	justify-content: center;
`;

```

```tsx
	<CalendarWrapper visible={st} delay={500}>
					<CalendarHeadWrapper>我是标题</CalendarHeadWrapper>
					<table>
						<thead>
							<tr>
								<CalendarHeadItem>日</CalendarHeadItem>
								<CalendarHeadItem>一</CalendarHeadItem>
								<CalendarHeadItem>二</CalendarHeadItem>
								<CalendarHeadItem>三</CalendarHeadItem>
								<CalendarHeadItem>四</CalendarHeadItem>
								<CalendarHeadItem>五</CalendarHeadItem>
								<CalendarHeadItem>六</CalendarHeadItem>
							</tr>
						</thead>
						<tbody>
							{dayData.map((v, index) => (
								<CalendarDateRow key={index}>
									{v.map((k, i) => (
										<CalendarDate key={i}>{k}</CalendarDate>
									))}
								</CalendarDateRow>
							))}
						</tbody>
					</table>
				</CalendarWrapper>
```

这样稍微好看点，然后使用数据进行替换，这里需要搞个函数，因为我们要通过表头上的切换月份年份来定位天。

所以我们这个函数是通过传入月份年份返回天数列表

```typescript
const getDateData = function(year: number, month: number) {
	const firstDay = new Date(year, month, 1);
	let weekDay = firstDay.getDay(); //周日，0，周六 6
	weekDay = weekDay === 0 ? 7 : weekDay;
	let start = firstDay.getTime() - weekDay * 60 * 60 * 24 * 1000;
	let arr: number[] = [];
	for (let i = 0; i < 42; i++) {
		arr.push(new Date(start + i * 60 * 60 * 24 * 1000).getDate());
	}
	let k = -1;
	return Array.from({ length: 6 }, () => {
		k++;
		return arr.slice(k * 7, (k + 1) * 7);
	});
};
```

```
const dayData = useMemo(() => {
		const arr = getDateData(2020, 8); //传的8实际是9

		return arr;
	}, []);
```

这样日历就做出来了，只要改变传入参数就能获得对应日历样式。

下面就得让日历可以切换，我们需要设置state，作为当前状态，然后修改标题部分，制作切换年月的小按钮：

```typescript
type calDataType = [number, number];

const btnStyle = {
	padding: "0px",
	background: color.lightest,
};
const IconWrapper = styled.span`
	display: inline-block;
	vertical-align: middle;
	> svg {
		height: 12px;
	}
`;

const BtnDiv = styled.div`
	display: flex;
	jutify-content: center;
	align-items: center;
	height: 24px;
	line-height: 24px;
`;
```

顺带加上2个click方法：

```tsx
const render = useMemo(() => {
		const handleLeft = () => {
			const res = changeCalData(true, calData);
			setCalData(res);
		};
		const handleRight = () => {
			const res = changeCalData(false, calData);
			setCalData(res);
		};
		if (!show) {
			unmount();
			return null;
		} else {
			return (
				<CalendarWrapper visible={st} delay={210}>
					<CalendarHeadWrapper>
						<BtnDiv>
							<Button
								size="small"
								style={btnStyle}
								onClick={() => handleLeft()}
							>
								<IconWrapper>
									<Icon icon="arrowleft"></Icon>
								</IconWrapper>
							</Button>
						</BtnDiv>
						<BtnDiv>{`${calData[0]}年${calData[1] + 1}月`}</BtnDiv>
						<BtnDiv>
							<Button
								size="small"
								style={btnStyle}
								onClick={() => {
									handleRight();
								}}
							>
								<IconWrapper>
									<Icon icon="arrowright"></Icon>
								</IconWrapper>
							</Button>
						</BtnDiv>
					</CalendarHeadWrapper>
					<table>
						<thead>
							<tr>
								<CalendarHeadItem>日</CalendarHeadItem>
								<CalendarHeadItem>一</CalendarHeadItem>
								<CalendarHeadItem>二</CalendarHeadItem>
								<CalendarHeadItem>三</CalendarHeadItem>
								<CalendarHeadItem>四</CalendarHeadItem>
								<CalendarHeadItem>五</CalendarHeadItem>
								<CalendarHeadItem>六</CalendarHeadItem>
							</tr>
						</thead>
						<tbody>
							{dayData.map((v, index) => (
								<CalendarDateRow key={index}>
									{v.map((k, i) => (
										<CalendarDate key={i}>{k}</CalendarDate>
									))}
								</CalendarDateRow>
							))}
						</tbody>
					</table>
				</CalendarWrapper>
			);
		}
	}, [show, unmount, st, calData, dayData]);
```

通过系统来计算年月：

```typescript
const getYearMonthDay = function(date: number): calDataType {
	let tmp = new Date(date);
	return [tmp.getFullYear(), tmp.getMonth()];
};

const changeCalData = function(
	sign: boolean,
	calData: calDataType
): calDataType {
	const oldDate = new Date(calData[0], calData[1]);
	if (sign) {
		//true是减少false是增加
		const newDate = oldDate.setMonth(oldDate.getMonth() - 1);
		return getYearMonthDay(newDate);
	} else {
		const newDate = oldDate.setMonth(oldDate.getMonth() + 1);
		return getYearMonthDay(newDate);
	}
};
```

这样就完成翻页了，可以试试如果左右正常翻页就ok。

下面需要对不在本月的修改颜色，这需要对前面那循环方法进行改写，使其包含更多信息，同时因为涉及日的计算，所以把state增加个日期：

```
type calDataType = [number, number,number];
```

```
const [calData, setCalData] = useState<calDataType>(() => [
		new Date().getFullYear(),
		new Date().getMonth(),
		new Date().getDate(),
	]);
```

```
const getYearMonthDay = function(date: number): calDataType {
	let tmp = new Date(date);
	return [tmp.getFullYear(), tmp.getMonth(),tmp.getDate()];
};
```

对循环进行修改：

```typescript
interface DateItem {
	day: number; //天
	isonMonth: boolean; //当月
	isonDay: boolean; //当日
	origin: Date;
}


const isCurrentMonth = function(
	current: Date,
	year: number,
	month: number
): boolean {
	return current.getFullYear() === year && current.getMonth() === month;
};
const isCurrentDay = function(current: Date, day: number, onMonth: boolean) {
	return current.getDate() === day && onMonth;
};

const getDateData = function(year: number, month: number, day: number) {
	const firstDay = new Date(year, month, 1);
	let weekDay = firstDay.getDay(); //周日，0，周六 6
	weekDay = weekDay === 0 ? 7 : weekDay;
	let start = firstDay.getTime() - weekDay * 60 * 60 * 24 * 1000;
	let arr: DateItem[] = [];
	for (let i = 0; i < 42; i++) {
		let current = new Date(start + i * 60 * 60 * 24 * 1000);
		let onMonth = isCurrentMonth(current, year, month);
		arr.push({
			day: current.getDate(),
			isonMonth: onMonth,
			isonDay: isCurrentDay(current, day, onMonth),
			origin: current,
		});
	}
	let k = -1;
	return Array.from({ length: 6 }, () => {
		k++;
		return arr.slice(k * 7, (k + 1) * 7);
	});
};

```

这样我们传入render的元素里就多了信息，然后传入styledComponents进行样式控制，同时增加点击事件，让其点击改变state：

```tsx
		{dayData.map((v, index) => (
								<CalendarDateRow key={index}>
									{v.map((k, i) => (
										<CalendarDate
												isonDay={k.isonDay}
											isonMonth={k.isonMonth}
											key={i}
											onClick={() => {
												const origin = k.origin;
												setCalData([
													origin.getFullYear(),
													origin.getMonth(),
													origin.getDate(),
												]);
												setst(false);
											}}
										>
											{k.day}
										</CalendarDate>
									))}
								</CalendarDateRow>
							))}
```

```typescript
const CalendarDate = styled.td<Partial<DateItem>>`
	display: inline-block;
	min-width: 24px;
	height: 24px;
	line-height: 24px;
	border-radius: 2px;
	margin: 2px;
	text-align: center;
	cursor: pointer;
		${(props) => {
		if (props.isonDay) {
			//当天的 
			return `color:${color.lightest};background:${color.primary};`;
		}
		return `&:hover {color: ${color.secondary};};`;
	}}
	${(props) => {
		if (props.isonMonth === false) {
			//不是当月显示灰色
			return `color:${color.mediumdark};`;
		}
		return "";
	}}
`;
```

这样就完成用户点击逻辑+底色修改，下面制作input框显示，由于我们有状态就是state，但是在挑选日期的时候，输入框里的值其实不应该变化的，所以这个输入框的值不能直接使用calData。

这样需要初始填充今天日期，然后，当用户点击选择时，把新值赋上：

```
const [state, setState] = useState(
		() => generateDate(calData)
	);
```

```
const generateDate=(calData:calDataType)=>{
	return `${calData[0]}-${calData[1] + 1}-${calData[2]}`
}
```

```tsx
	<CalendarDate
        isonDay={k.isonDay}
        isonMonth={k.isonMonth}
        key={i}
        onClick={() => {
        const origin = k.origin;
        const newCal: calDataType = [
            origin.getFullYear(),
            origin.getMonth(),
            origin.getDate(),
        ];
        setCalData(newCal);
        setState(generateDate(newCal));
        setst(false);
    }}
        >
    {k.day}
</CalendarDate>
```

这样就完成了 ，可以试一下，输入框可以正确显示内容了，并且翻页输入框不会变化。

下面需要对输入框的值进行转换。

比如我在输入框输入2020-8-11，blur后，日历需要直接跳到811上。如果我输入的值不满足格式，那么就会重新显示最后一次正确的格式。

由于涉及到日期校验，所以需要写个方法：

```typescript
const validateDate = (value: string) => {
	let reg = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;
	if (reg.exec(value)) {
		return true;
	} else {
		return false;
	}
};
```

然后制作blur事件：

```typescript
const handleBlur = () => {
		if (state !== generateDate(calData)) {
			//如果相等，说明是calData赋值上去的
			let res = validateDate(state); //验证格式
			if (!res) {
				//错误用原来的
				setState(generateDate(calData));
			} else {
				//否则计算新值
				let p = state.split("-");
				let newDate = new Date(
					parseInt(p[0]),
					parseInt(p[1]) - 1,
					parseInt(p[2])
				);
				const newCal: calDataType = [
					newDate.getFullYear(),
					newDate.getMonth(),
					newDate.getDate(),
				];
				setCalData(newCal);
				setState(generateDate(newCal));
			}
		}
	};
```

```tsx
	<input
        aria-label="date picker"
        onBlur={handleBlur}
        value={state}
        onChange={handleChange}
        onClick={handleClick}
        ></input>
```

可以试验下，输入框里瞎写或者正常写，然后再看日历是否对应正常。

下面制作下一个功能，我们要进行切换模式，同时换掉日期显示。

这样，我们会有3种模式，日期模式，月份模式，年份模式。

首先，就是内部制作状态，制作3种模式。

```
type modeType = 'date'|'month'|'year'
```

```
	const [mode,setMode]=useState<modeType>('date')
```

然后，需要把日期那表格提出去：

````tsx
	const modeDay = (
			<table style={{ display: mode === "month" ? "flex" : "none" }}>
				<thead>
					<tr>
						<CalendarHeadItem>日</CalendarHeadItem>
						<CalendarHeadItem>一</CalendarHeadItem>
						<CalendarHeadItem>二</CalendarHeadItem>
						<CalendarHeadItem>三</CalendarHeadItem>
						<CalendarHeadItem>四</CalendarHeadItem>
						<CalendarHeadItem>五</CalendarHeadItem>
						<CalendarHeadItem>六</CalendarHeadItem>
					</tr>
				</thead>
				<tbody>
					{dayData.map((v, index) => (
						<CalendarDateRow key={index}>
							{v.map((k, i) => (
								<CalendarDate
									isonDay={k.isonDay}
									isonMonth={k.isonMonth}
									key={i}
									onClick={() => {
										const origin = k.origin;
										const newCal: calDataType = [
											origin.getFullYear(),
											origin.getMonth(),
											origin.getDate(),
										];
										setCalData(newCal);
										setState(generateDate(newCal));
										setst(false);
									}}
								>
									{k.day}
								</CalendarDate>
							))}
						</CalendarDateRow>
					))}
				</tbody>
			</table>
		);
````

修改返回组件：

```tsx
	return (
				<CalendarWrapper visible={st} delay={210}>
					<CalendarHeadWrapper>
						<div
							style={{
								display: "flex",
								justifyContent: "center",
							}}
						>
							<BtnDiv>
								<Button
									size="small"
									style={btnStyle}
									onClick={() => handleLeft()}
								>
									<IconWrapper>
										<Icon icon="arrowleft"></Icon>
									</IconWrapper>
								</Button>
							</BtnDiv>
							<BtnDiv>{`${calData[0]}年${calData[1] +
								1}月`}</BtnDiv>
							<BtnDiv>
								<Button
									size="small"
									style={btnStyle}
									onClick={() => handleRight()}
								>
									<IconWrapper>
										<Icon icon="arrowright"></Icon>
									</IconWrapper>
								</Button>
							</BtnDiv>
						</div>

						<div style={{ padding: "10px" }}>
								{modeDay}
							{modeMonth}
						</div>
					</CalendarHeadWrapper>
				</CalendarWrapper>
			);
		}
	}, [show, unmount, st, calData, dayData, setst, mode]);
```

下面要进行切换模式，通常是点击上面那个年，切换年，点击上面那个月，切换月，先制作月模式。

在月模式下，上方文本应该换成哪一年，同时，左右2个按键应该可以调整年份，下方表格应该显示12个月。

先制作调整年份的方法，为了复用月份修改，把月份改成按number传递：

```typescript
const changeCalData = function(
	sign: number,
	calData: calDataType
): calDataType {
	const oldDate = new Date(calData[0], calData[1]);
	const newDate = oldDate.setMonth(oldDate.getMonth() +sign);
	return getYearMonthDay(newDate);
};

const changeCalYear = function(sign: number, calData: calDataType) {
	const oldDate = new Date(calData[0], calData[1]);
	const newDate = oldDate.setFullYear(oldDate.getFullYear()+ sign);
	return getYearMonthDay(newDate);
};

```

然后改写中间部分，加上切换mode事件。

这里有个坑就是这里不能使用&&形式判断渲染，因为useClickOutside钩子会对你这么卸载的直接判断关闭，然后把弹窗关闭了，所以使用display:none的方法来进行切换。

```
							<BtnDiv>
								<span>
									<b
										onClick={() => {
											setMode("year");
										}}
									>{`${calData[0]}年`}</b>
									<b
										onClick={() => {
											setMode("month");
										}}
										style={{
											display:
												mode === "date"
													? "inline-block"
													: "none",
										}}
									>{`${calData[1] + 1}月`}</b>
								</span>
							</BtnDiv>
```

制作12个月：

```
const MonthData = new Array(12).fill(1).map((_x, y) => y + 1);
```

增加点击事件：

```tsx
	const modeMonth = (
			<div style={{ display: mode === "month" ? "flex" : "none" }}>
				{MonthData.map((v, i) => {
					return (
						<div
							key={i}
							onClick={() => {
								//获取当前月，与点击相减得差
								let diff = v - calData[1] - 1;
								let res = changeCalData(diff, calData);
								setCalData(res);
								setMode("date");
							}}
						>
							{v}月
						</div>
					);
				})}
			</div>
		);
```

为了样式好看一点，使用styledComponents处理下：

```typescript
const MonthWrapper = styled.div`
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	position: relative;
`;

const MonthItem = styled.div`
	width: 25%;
	height: 60px;
	display: flex;
	justify-content: center;
	align-items: center;
	cursor: pointer;
	&:hover {
		color: ${color.secondary};
	}
`;
const Bwrapper = styled.b`
	cursor: pointer;
	&:hover {
		color: ${color.primary};
	}
`;
```

```tsx
	const modeMonth = (
			<MonthWrapper
				style={{ display: mode === "month" ? "flex" : "none" }}
			>
				{MonthData.map((v, i) => {
					return (
						<MonthItem
							key={i}
							onClick={() => {
								//获取当前月，与点击相减得差
								let diff = v - calData[1] - 1;
								let res = changeCalData(diff, calData);
								setCalData(res);
								setMode("date");
							}}
						>
							{v}月
						</MonthItem>
					);
				})}
			</MonthWrapper>
		);
```

```tsx
return (
				<CalendarWrapper visible={st} delay={210}>
					<CalendarHeadWrapper>
						<div
							style={{
								display: "flex",
								justifyContent: "center",
								width: "240px",
							}}
						>
							<BtnDiv style={{ marginLeft: "20px" }}>
								<Button
									size="small"
									style={btnStyle}
									onClick={() => handleLeft()}
								>
									<IconWrapper>
										<Icon icon="arrowleft"></Icon>
									</IconWrapper>
								</Button>
							</BtnDiv>
							<BtnDiv style={{ flex: 1 }}>
								<span>
									<Bwrapper
										onClick={() => {
											setMode("year");
										}}
									>{`${calData[0]}年`}</Bwrapper>
									<Bwrapper
										onClick={() => {
											setMode("month");
										}}
										style={{
											display:
												mode === "date"
													? "inline-block"
													: "none",
										}}
									>{`${calData[1] + 1}月`}</Bwrapper>
								</span>
							</BtnDiv>
							<BtnDiv style={{ marginRight: "20px" }}>
								<Button
									size="small"
									style={btnStyle}
									onClick={() => handleRight()}
								>
									<IconWrapper>
										<Icon icon="arrowright"></Icon>
									</IconWrapper>
								</Button>
							</BtnDiv>
						</div>

						<div
							style={{
								width: "240px",
								display: "flex",
								justifyContent: "center",
							}}
						>
							{modeDay}
							{modeMonth}
						</div>
					</CalendarHeadWrapper>
				</CalendarWrapper>
```

这样月份就制作完成，点击月份然后可以切换月模式，切换完再点击可以回到日期模式等待选择。

下面制作年模式。

年模式需要上方文本返回一个范围，同时左右切换会以年的上限进行切换，这里为了保持和月份统一，每页显示12个，其中，有效显示是10个，另外2个是凑数的。

方法：

```
const getStartYear =function(calData: calDataType){
	return calData[0]-calData[0]%10
}
```

复用month的styledcomponent，增加变量：

```
const MonthItem = styled.div<{ toGrey?: boolean }>`
	width: 25%;
	height: 60px;
	display: flex;
	justify-content: center;
	align-items: center;
	cursor: pointer;
	${(props) => props.toGrey && `color:${color.mediumdark};`}
	&:hover {
		color: ${color.secondary};
	}
`;
```

制作年模式渲染：

```tsx
	const startYear = getStartYear(calData);
		const yearMap = new Array(12).fill(1).map((_x, y) => startYear + y - 1);
		const modeYear = (
			<MonthWrapper
				style={{ display: mode === "year" ? "flex" : "none" }}
			>
				{yearMap.map((v, i) => (
					<MonthItem
						toGrey={i === 0 || i === 11}
						key={i}
						onClick={() => {
							//获取选择的年与差值
							let diff = v - calData[0];
							let res = changeCalYear(diff, calData);
							setCalData(res);
							setMode("month");
						}}
					>
						{v}
					</MonthItem>
				))}
			</MonthWrapper>
		);
```

左右键的处理逻辑：

````typescript
	const handleLeft = () => {
			let res: calDataType;
			if (mode === "date") {
				res = changeCalData(-1, calData);
			} else if (mode === "month") {
				res = changeCalYear(-1, calData);
			} else {
				res = changeCalYear(-10, calData);
			}
			setCalData(res);
		};
		const handleRight = () => {
			let res: calDataType;
			if (mode === "date") {
				res = changeCalData(1, calData);
			} else if (mode === "month") {
				res = changeCalYear(1, calData);
			} else {
				res = changeCalYear(10, calData);
			}
			setCalData(res);
		};
````

渲染部分：

```tsx
	return (
				<CalendarWrapper visible={st} delay={210}>
					<CalendarHeadWrapper>
						<div
							style={{
								display: "flex",
								justifyContent: "center",
								width: "240px",
							}}
						>
							<BtnDiv style={{ marginLeft: "20px" }}>
								<Button
									size="small"
									style={btnStyle}
									onClick={() => handleLeft()}
								>
									<IconWrapper>
										<Icon icon="arrowleft"></Icon>
									</IconWrapper>
								</Button>
							</BtnDiv>
							<BtnDiv style={{ flex: 1 }}>
								<span>
									<Bwrapper
										style={{
											display:
												mode === "year"
													? "inline-block"
													: "none",
										}}
									>{`${startYear}-${startYear +
										9}`}</Bwrapper>
									<Bwrapper
										onClick={() => {
											setMode("year");
										}}
										style={{
											display:
												mode === "month" ||
												mode === "date"
													? "inline-block"
													: "none",
										}}
									>{`${calData[0]}年`}</Bwrapper>
									<Bwrapper
										onClick={() => {
											setMode("month");
										}}
										style={{
											display:
												mode === "date"
													? "inline-block"
													: "none",
										}}
									>{`${calData[1] + 1}月`}</Bwrapper>
								</span>
							</BtnDiv>
							<BtnDiv style={{ marginRight: "20px" }}>
								<Button
									size="small"
									style={btnStyle}
									onClick={() => handleRight()}
								>
									<IconWrapper>
										<Icon icon="arrowright"></Icon>
									</IconWrapper>
								</Button>
							</BtnDiv>
						</div>

						<div
							style={{
								width: "240px",
								display: "flex",
								justifyContent: "center",
							}}
						>
							{modeDay}
							{modeMonth}
							{modeYear}
						</div>
					</CalendarHeadWrapper>
				</CalendarWrapper>
			);
```

这样就完成了3种模式切换。

为了让输入框更具有标识性，我们给输入框加个icon，同时隐藏input一些默认样式：

```tsx
<DatePickerWrapper ref={ref} onClick={handleClick}>
			<input
				aria-label="date picker"
				onBlur={handleBlur}
				value={state}
				onChange={handleChange}
				style={{ border: "none", boxShadow: "none", outline: "none" }}
			></input>
			<CalendarIcon>
				<Icon icon="calendar"></Icon>
			</CalendarIcon>
			{render}
		</DatePickerWrapper>
```

```typescript
const CalendarIcon = styled.span`
	display: inline-block;
`;

const DatePickerWrapper = styled.div`
	display: inline-block;
	border-color: #40a9ff;
	border-right-width: 1px !important;
	outline: 0;
	box-shadow: 0 0 0 2px rgba(24, 144, 255, 0.2);
`;
```

这样就完成了。

最后就是暴露些配置项与story了，相信写了这么多都会写了。我就简单写几个接口：其中callback是不能没有的，因为日期选择肯定要拿到选了啥：

```
export type DatepickerProps = {
	/** 日期选择的回调 */
	callback?: (v: string) => void;
	/**  动画速度 */
	delay?: number;
	/** 初始值*/
	initDate?: string;
	/** 外层样式*/
	style?: CSSProperties;
	/** 外层类名 */
	classname?: string;
};

```

```
	useEffect(() => {
		if (callback) callback(state);
	}, [state, callback]);
```

这就搞定了。

初始值可以开头判断下：

```
	const [state, setState] = useState(() => {
		if (initDate && validateDate(initDate)) {
			return initDate;
		} else {
			return generateDate(calData);
		}
	});
```

最后配置下story：

```typescript
export const knobsDatePicker = () => (
	<div style={{ height: "500px" }}>
		<DatePicker
			callback={action("callback")}
			delay={number("delay", 200)}
			initDate={text("initDate", "")}
		></DatePicker>
	</div>
);
```

## 关于Ref

对于有input之类的特殊组件，其实最好应该暴露其ref，因为可以添加focus之类东西，如果专门提供方法接口也是可以的，当然不暴露用的人也有很多种方法可以获取到，暴露出来会比较方便。函数组件一般不需要用forwardRef包裹，推荐使用回调方式暴露ref。



## 今日作业

完成DatePicker组件
